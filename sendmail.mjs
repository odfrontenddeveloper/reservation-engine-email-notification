import fs from 'fs'
import nodemailer from 'nodemailer'

const main = () => {
    const from = 'nodemailerodessatest@gmail.com'
    const pass = 'nodemailer'
    const to = '0933931313q@gmail.com'
    const subject = 'HTML code email testing'

    const existsHTML = fs.existsSync('./build/index.html')

    if (!existsHTML) {
        console.log('HTML file is not defined!')
        return false
    }

    const htmlfile = fs.readFileSync('./build/index.html')

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: from,
            pass,
        },
    })

    const mailOptions = {
        from,
        to,
        subject,
        html: htmlfile,
    }

    console.log('Sending...')

    transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            console.log('ERROR!', err)
            res.send(err)
        } else {
            console.log(`Message sent! Check email <${to}>.`)
            res.send('Message sent!')
        }
    })
}

main()

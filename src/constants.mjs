//tasks
export const HTML_CODE = 'HTML_CODE'
export const SASS_TO_CSS = 'SASS_TO_CSS'
export const SASS_TO_CSS_MIN = 'SASS_TO_CSS_MIN'
export const HOT_RELOAD = 'HOT_RELOAD'
export const COMPRESS = 'COMPRESS'

//general tasks
export const DEV_MODE = 'DEV_MODE'
export const PROD_MODE = 'PROD_MODE'

//build
export const BUILD_PATH = './build'

//pug
export const PUG_SOURCE_PATH = './src/pug/index.pug'
export const PUG_FILES_PATH = './src/pug/*.pug'

//sass
export const SCSS_SOURCE_PATH = './src/scss/index.scss'
export const CSS_DEST_PATH = './src/css'
export const SCSS_FILES_PATH = './src/scss/*.scss'

//images
export const IMAGES_SOUCE_PATH = './src/images/*'
export const IMAGES_DEST_PATH = './build/images'
